<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modificación de un Estudiante</title>
</head>

<body>
    <h1>Modificación de un estudiante</h1>
    <?php
    include("../clases/estudiante.php");
    include("../clases/conexion.php");

    $conexion = new Conexion();
    try {
        $conexion->abrir();
    } catch (Exception $e) {
        echo "EXCEPCIÓN CAPTURADA: " . $e->getMessage();
    }

    if ($row = $conexion->consultaEstudiante($_POST["ci"])->fetch_array()) {
    ?>
        <form action="./modificado.php" method="post">
            <label for="ci">Cédula de Identidad: </label>
            <input type="text" name="ci" id="ci" placeholder="9.999.999-9" value="<?php echo $row["ci"]; ?>" readonly>
            <br><br>
            <label for="name">Nombre: </label>
            <input type="text" name="name" id="name" placeholder="Juan" value="<?php echo $row["nombre"]; ?>" readonly>
            <br><br>
            <label for="lastName">Apellido: </label>
            <input type="text" name="lastName" id="lastName" placeholder="Pérez" value="<?php echo $row["apellido"]; ?>" readonly>
            <br><br>
            <label for="age">Edad: </label>
            <input type="number" name="age" id="age" min="17" value="<?php echo $row["edad"]; ?>" required>
            <br><br>
            <button type="submit">Agregar</button>
            <button type="reset">Cancelar</button>
        </form>
    <?php
    } else {
    ?>
        <h1>Oops! Parece que el estudiante que intentas modificar no está registrado.</h1>
        <h3>Vuelve al menú para volver a intentarlo.</h3>
    <?php
    }
    $conexion->cerrar();
    ?>
    <br><br>
    <a href="../index.html">Volver al menú</a>
</body>

</html>