<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modificar Datos de un Estudiante</title>
</head>

<body>
    <?php
    include("../clases/estudiante.php");
    include("../clases/conexion.php");
    //Se abre la conexión
    $conexion = new Conexion();
    try {
        $conexion->abrir();
    } catch (Exception $e) {
        echo "EXCEPCIÓN CAPTURADA: " . $e->getMessage();
    }

    $estudiante = new Estudiante($_POST["ci"], $_POST["name"], $_POST["lastName"], $_POST["age"]);

    if ($conexion->modificacionEstudiante($estudiante) === TRUE) {
    ?>
        <h1>Estudiante modificado con éxito!</h1>
    <?php
    } else {
    ?>
        <h1>Oops! Algo salió mal...</h1>
    <?php
    }
    $conexion->cerrar();
    ?>
    <br><br>
    <a href="../index.html">Volver al menú</a>
</body>

</html>