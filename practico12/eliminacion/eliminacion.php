<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ELiminación de un estudiante</title>
</head>

<body>
    <?php
    include("../clases/estudiante.php");
    include("../clases/conexion.php");
    //Se abre la conexión
    $conexion = new Conexion();
    try {
        $conexion->abrir();
    } catch (Exception $e) {
        echo "EXCEPCIÓN CAPTURADA: " . $e->getMessage();
    }

    if ($row = $conexion->consultaEstudiante($_POST["ci"])->fetch_array()) {
        if ($conexion->eliminacionEstudiante($_POST["ci"]) === TRUE) {
    ?>
            <h1><?php echo "Estudiante ".$row["nombre"]." ".$row["apellido"].", ci : " . $_POST["ci"] . ", eliminado correctamente." ?></h1>
        <?php
        } else {
        ?>
            <h1>Oops! Algo salió mal...</h1>
        <?php
        }
    } else {
        ?>
        <h1>El estudiante no se encuentra registrado.</h1>
    <?php
    }
    $conexion->cerrar();
    ?>
    <br><br>
    <a href="../index.html">Volver al menú</a>
</body>

</html>