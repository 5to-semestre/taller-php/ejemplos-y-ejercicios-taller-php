<?php
class Estudiante {
    private $data = array();
    private $ci;
    private $nombre;
    private $apellido;
    private $edad;

    public function __construct($ci, $nombre, $apellido, $edad)
    {
        $this->ci = $ci;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->edad = $edad;
    }

    public function getCi() : string {
        return $this->ci;
    }
    
    public function setCi($ci) : void {
        $this->ci;
    }

    public function getNombre() : string {
        return $this->nombre;
    }
    
    public function setNombre($ci) : void {
        $this->nombre;
    }

    public function getApellido() : string {
        return $this->apellido;
    }
    
    public function setApellido($ci) : void {
        $this->apellido;
    }

    public function getEdad() : int {
        return $this->edad;
    }
    
    public function setEdad($ci) : void {
        $this->edad;
    }
}
?>