<?php
include("../lib/constantesConexion.php");

class Conexion {
    private $conexion;

    private function conectar() {
        $this->conexion = new mysqli(SERVIDOR, USUARIO, CONTRASENA, BD) or die ("ERROR DE LA CONEXIÓN");
        if($this->conexion->connect_errno) {
            throw new Exception(
                $this->conexion->connect_error." - Nro de error: ".$this->conexion->connect_errno
            );
        }
        // return $this->conexion;
    }

    public function abrir() {
        $this->conectar();
    }

    public function cerrar() {
        $this->conexion->close();
    }

    private function consultaSql($sql, $resultmode = MYSQLI_STORE_RESULT) {
        return $this->conexion->query($sql, $resultmode);
    }
    
    public function cantRegistros($sql) {
        return $this->consultaSql($sql)->num_rows;
    }

    public function consultaEstudiante($ci) {
        $sql = "SELECT * FROM estudiante WHERE ci = '".$ci."';";
        return $this->consultaSql($sql);
    }

    public function ingresoEstudiante($est) {
        $sql="INSERT INTO estudiante (ci, nombre, apellido, edad) VALUES ('".$est->getCi()."','".$est->getNombre()."','".$est->getApellido()."',".$est->getEdad().");";
        return $this->consultaSql($sql);
    }

    public function eliminacionEstudiante($ci) {
        $sql = "DELETE FROM estudiante WHERE ci = '".$ci."';";
        return $this->consultaSql($sql);
    }

    public function modificacionEstudiante($est) {
        $sql = "UPDATE estudiante SET edad = '".$est->getEdad()."' WHERE ci = '".$est->getCi()."';";
        return $this->consultaSql($sql);
    }

    public function listadoEstudiantes() {
        $sql = "SELECT * FROM estudiante ORDER BY nombre ASC, apellido ASC;";
        return $this->consultaSql($sql, MYSQLI_USE_RESULT);
    }
}
?>