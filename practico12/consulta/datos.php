<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consulta de un Estudiante</title>
</head>

<body>
    <?php
    include("../clases/estudiante.php");
    include("../clases/conexion.php");

    $conexion = new Conexion();
    try {
        $conexion->abrir();
    } catch (Exception $e) {
        echo "EXCEPCIÓN CAPTURADA: " . $e->getMessage();
    }

    if ($row = $conexion->consultaEstudiante($_POST["ci"])->fetch_array()) {
    ?>
        <h1>Datos del estudiante</h1>
        <label for="ci">Cédula de Identidad: </label>
        <input type="text" value="<?php echo $row["ci"]; ?>" readonly>
        <br><br>
        <label for="name">Nombre: </label>
        <input type="text" value="<?php echo $row["nombre"]; ?>" readonly>
        <br><br>
        <label for="lastName">Apellido: </label>
        <input type="text" value="<?php echo $row["apellido"]; ?>" readonly>
        <br><br>
        <label for="age">Edad: </label>
        <input type="number" value="<?php echo $row["edad"]; ?>" readonly>
    <?php
    } else {
    ?>
        <h1>Oops! Parece que el estudiante que intentas consultar no está registrado.</h1>
        <h3>Vuelve al menú para volver a intentarlo.</h3>
    <?php
    }
    $conexion->cerrar();
    ?>
    <br><br><br><br>
    <a href="../index.html">Volver al menú</a>
</body>

</html>