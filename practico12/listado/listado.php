<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../lib/bootstrap.min.css">
    <title>Listado de Estudiantes</title>
</head>

<body>
    <h1 class="text-center mb-5 mt-5">Listado de Estudiantes</h1>
    <?php
    include("../clases/estudiante.php");
    include("../clases/conexion.php");

    $conexion = new Conexion();
    try {
        $conexion->abrir();
    } catch (Exception $e) {
        echo "EXCEPCIÓN CAPTURADA: " . $e->getMessage();
    }
    $result = $conexion->listadoEstudiantes();
    ?>
    <div class="row justify-content-center align-items-center">
        <div class="col-10">
            <table class="table table-hover">
                <thead class="table-light">
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>C.I.</th>
                        <th>Edad</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($row = $result->fetch_array()) {
                    ?>
                        <tr>
                            <td><?php echo $row["nombre"] ?></td>
                            <td><?php echo $row["apellido"] ?></td>
                            <td><?php echo $row["ci"] ?></td>
                            <td><?php echo $row["edad"] ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
                <tfoot class="table-light text-center">
                    <tr>
                        <td colspan="4"><?php echo "Total: " . $result->num_rows ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <?php
    $result->close();
    $conexion->cerrar();
    ?>
    <div class="row justify-content-center align-items-center mt-5">
        <div class="col-auto">
            <a href="../index.html" class="btn btn-secondary">Volver al menú</a>
        </div>
    </div>
</body>

</html>