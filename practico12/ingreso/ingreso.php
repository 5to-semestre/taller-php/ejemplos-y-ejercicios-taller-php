<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingreso de estudiante</title>
</head>

<body>
    <?php
    include("../clases/estudiante.php");
    include("../clases/conexion.php");
    //Se abre la conexión
    $conexion = new Conexion();
    try {
        $conexion->abrir();
    } catch (Exception $e) {
        echo "EXCEPCIÓN CAPTURADA: " . $e->getMessage();
    }

    $estudiante = new Estudiante($_POST["ci"], $_POST["name"], $_POST["lastName"], $_POST["age"]);
    if ($row = $conexion->consultaEstudiante($_POST["ci"])->fetch_array()) {
    ?>
        <h1>El estudiante ya se encuentra registrado.</h1>
        <?php
    } else {
        if ($conexion->ingresoEstudiante($estudiante) === TRUE) {
        ?>
            <h1>Ingreso exitoso!</h1>
        <?php
        } else {
        ?>
            <h1>Oops! Algo salió mal...</h1>
    <?php
        }
    }
    $conexion->cerrar();
    ?>
    <br><br>
    <a href="../index.html">Volver al menú</a>
</body>

</html>