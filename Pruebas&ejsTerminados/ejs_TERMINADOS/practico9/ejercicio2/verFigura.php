<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>
<body>
    <h1>Figura elegida</h1>
        <?php
        // Matriz de figuras, en el primer array están los nombres y en el segundo las rutas de las imágenes.
        $figuras = array(array(
            "Círculo", "Cuadrado", "Hexágono","Rectángulo", "Rombo", "Trapecio Isóceles", "Triángulo"),
            array(
                './imagenes/circulo.png', './imagenes/cuadrado.png', './imagenes/hexagono.png', './imagenes/rectangulo.png', './imagenes/rombo.png', './imagenes/trapecioIsosceles.png', './imagenes/triangulo.png')
        );
        ?>
        <figure>
            <img src="<?php echo $figuras[1][$_POST["figura"]]; ?>" alt="Figura elegida.">
            <figcaption>Nombre: <?php echo $figuras[0][$_POST["figura"]]; ?></figcaption>
        </figure>

        <br /><br />
        <button><a href="./ejercicio2.html">Elegir otra figura</a></button>
        <button><a href="../index.html">Volver al menú</a></button>
</body>
</html>