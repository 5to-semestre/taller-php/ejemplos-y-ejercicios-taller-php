<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>
<body>
    <?php
    $grupos = array(array(), array());

    for($x=0; $x < $_POST["cantGrupos"]; $x++){
        $materia = "materia" . ($x + 1);
        $cantidad = "cantidad" . ($x + 1);
        
        $grupos[0][$x] = $_POST[$materia];
        $grupos[1][$x] = $_POST[$cantidad];
    ?>
        <div><?php echo "Grupo ", $x + 1, " - Materia: ", $grupos[0][$x], " - Cantidad: ", $grupos[1][$x]; ?></div>
        <br>
    <?php
    }
    ?>
    <br><br>
    <a href="../index.html">Volver al menú</a>
</body>
</html>