<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>
<body>
    <form action="./verDatos.php" method="post">
    <input type="hidden" name="cantGrupos" id="cantGrupos" value="<?php echo $_POST["cantGrupos"]; ?>">
        <?php
        for($i=1; $i <= $_POST["cantGrupos"]; $i++){
        ?>
            <label for="materia<?php echo $i?>">Grupo <?php echo $i ?>: Materia: </label>
            <input type="text" name="materia<?php echo $i?>" id="materia<?php echo $i?>" required>
            <label for="cantidad<?php echo $i?>">Cantidad: </label>
            <input type="number" name="cantidad<?php echo $i?>" id="cantidad<?php echo $i?>" min="0" required>
            <br><br>
        <?php
        }
        ?>
        <input type="reset" name="cancelar" id="cancelar" value="Cancelar">
        <input type="submit" name="aceptar" id="aceptar" value="Aceptar">
    </form>
    <br><br>
    <a href="../index.html">Volver al menú</a>
</body>
</html>