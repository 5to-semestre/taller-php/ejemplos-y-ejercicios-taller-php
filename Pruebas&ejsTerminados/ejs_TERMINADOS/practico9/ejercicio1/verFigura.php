<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1</title>
</head>
<body>
    <h1>Figura elegida</h1>
        <?php
        //Datos cargados en el array siguiendo la siguiente estructura:
        // 'Valor de opción del select' => 'Ruta de imagen'
        $figuras = array(
            'circulo' => './imagenes/circulo.png',
            'cuadrado' => './imagenes/cuadrado.png',
            'hexagono' => './imagenes/hexagono.png',
            'rectangulo' => './imagenes/rectangulo.png',
            'rombo' => './imagenes/rombo.png',
            'trapecio_isoceles' => './imagenes/trapecioIsosceles.png',
            'triangulo' => './imagenes/triangulo.png'
        );
        ?>
        <figure>
            <img src="<?php echo $figuras[$_POST["figura"]]; ?>" alt="Figura elegida.">
            <figcaption><?php echo $_POST["figura"]; ?></figcaption>
        </figure>

        <br /><br />
        <button><a href="./ejercicio1.html">Elegir otra figura</a></button>
        <button><a href="../index.html">Volver al menú</a></button>
</body>
</html>