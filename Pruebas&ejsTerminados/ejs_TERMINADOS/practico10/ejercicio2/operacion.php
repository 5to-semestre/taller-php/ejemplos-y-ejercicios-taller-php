<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2</title>
</head>

<body>
    <p>
        <?php
        include "../bibliotecas_php/funciones.php";
        echo "Resultado: ".OPERO($_POST["n1"], $_POST["n2"], $_POST["op"]);
        ?>
    </p>
    <br>
    <button><a href="./ejercicio2.html">Hacer otra cuenta</a></button>
    <button><a href="../index.html">Volver al menú</a></button>
</body>

</html>