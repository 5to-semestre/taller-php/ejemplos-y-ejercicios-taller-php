<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <?php
    include "../bibliotecas_php/funciones.php";
    $meses = MES_2($_POST["mes1"], $_POST["mes2"]);
    $largoMeses = count($meses);
    ?>
    <h1><?php echo "Meses comprendidos entre ", $_POST["mes1"], " y ", $_POST["mes2"], ": "; ?></h1>
    <p>
        <?php
        for ($i = 0; $i < $largoMeses; $i++) {
            echo $meses[$i];
            $largoMesesCond = $largoMeses - 1;
            if ($i < $largoMesesCond)
                echo " - ";
            else
                echo ".";
        ?>
        <?php
        }
        ?>
    </p>
    <br><br>
    <a href="../index.html">Vovler al menú</a>
</body>

</html>