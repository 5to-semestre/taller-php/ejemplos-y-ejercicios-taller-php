<?php
    function mayorNum($nros) {
        return max($nros);
    }

    function OPERO($nro1, $nro2, $op) {
        switch($op) {
            case "div":
                return $nro1 / $nro2;
                break;
            case "mul":
                return $nro1 * $nro2;
                break;
            case "sum":
                return $nro1 + $nro2;
                break;
            case "res":
                return $nro1 - $nro2;
                break;
            default:
                return null;
        }
    }

    function MES($numMes) {
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $numMes--;
        return $meses[$numMes];
    }

    function MES_2($mes1, $mes2) {
        $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $mesesReturn = array();
        $mes1--;
        $mes2--;
        $n = 0;
        for($i = $mes1; $i <= $mes2; $i++){
            $mesesReturn[$n] = $meses[$i];
            $n++;
        }
        return $mesesReturn;
    }
