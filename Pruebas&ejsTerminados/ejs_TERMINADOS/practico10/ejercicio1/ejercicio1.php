<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
        include "../bibliotecas_php/funciones.php";
        $arr = array(12, 2, 3, 4);
        echo "Valores en el arreglo: " . implode(" - ", $arr);
        echo "<br>";
        echo "Número mayor: " . mayorNum($arr);
    ?>
    <br>
    <br>
    <a href="../index.html">Volver al inicio</a>
</body>

</html>