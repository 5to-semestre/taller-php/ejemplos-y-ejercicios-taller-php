<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <!-- nombre apellido le corresponde cobrar por sus horas trabajadas xxxxxxxx pesos -->
    <?php
    $aumentoXHExtra = 1.5;
    $nombre = $_POST["name"];
    $apellido = $_POST["lName"];
    $horasT = $_POST["horasT"];
    $horasValor = $_POST["horasValor"];
    $horasExtra = $_POST["horasExtra"];

    //Valor del aumento por hora extra.
    $aumentoHValor = $horasValor * $aumentoXHExtra;

    //Valor de las horas extra trabajadas.
    $hValorXhExtras = $horasExtra * $aumentoHValor;

    //Valor de las horas trabajadas sin horas extra.
    $hValorXhTrabajadas = $horasT * $horasValor;

    //Valor total de horas trabajadas y horas extras.
    $horasValorTotal = $hValorXhExtras + $hValorXhTrabajadas;
    ?>
    <p><?php echo "$nombre $apellido le corresponde cobrar por sus horas trabajadas $horasValorTotal pesos." ?></p>
    <a href="../index.html">Volver al menú</a>
</body>

</html>