<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <p>
        <?php
        switch ($_POST["mes"]) {
            case 1:
                echo "Enero tiene 31 días.";
                break;
            case 2:
                echo "Febrero tiene 28/29 días.";
                break;
            case 3:
                echo "Marzo tiene 31 días.";
                break;
            case 4:
                echo "Abril tiene 30 días.";
                break;
            case 5:
                echo "Mayo tiene 31 días.";
                break;
            case 6:
                echo "Junio tiene 30 días.";
                break;
            case 7:
                echo "Julio tiene 31 días.";
                break;
            case 8:
                echo "Agosto tiene 31 días.";
                break;
            case 9:
                echo "Septiembre tiene 30 días.";
                break;
            case 10:
                echo "Octubre tiene 31 días.";
                break;
            case 11:
                echo "Noviembre tiene 30 días.";
                break;
            case 12:
                echo "Diciembre tiene 31 días.";
                break;
        }
        ?>
    </p>
    <br/>
    <br/>
    <a href="../index.html">Volver al menú</a>
</body>

</html>