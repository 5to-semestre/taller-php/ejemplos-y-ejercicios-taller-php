<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table, td {
            border: 1px solid #000000;
            text-align: center;
        }
        .negrita {
            font-weight: bold;
        }
    </style>
    <title>Ejercicio 5</title>
</head>

<body>
    <h1>Lista de alumnos:</h1>
    <table>
        <thead>
            <tr>
                <th>Nombre de Alumno</th>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 1; $i <= $_POST["cantAlumnos"]; $i++) {
                $input = "nomAlumno" . ($i);
            ?>
                <tr>
                    <td><?php echo $_POST[$input]; ?></td>
                </tr>
            <?php
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="negrita">Total: <?php echo $_POST["cantAlumnos"]; ?></td>
            </tr>
        </tfoot>
    </table>

    <br />
    <br />
    <a href="../index.html">Volver al menú</a>
</body>

</html>