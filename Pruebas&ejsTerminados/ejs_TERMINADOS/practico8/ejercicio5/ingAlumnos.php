<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <h1>Ingrese los nombres de cada alumno:</h1>
    <form action="./listarAlumnos.php" method="post">
        <input type="hidden" name="cantAlumnos" id="cantAlumnos" value="<?php echo $_POST["cantAlumnos"]; ?>">
        <?php
        for ($i = 1; $i <= $_POST["cantAlumnos"]; $i++) {
        ?>
            <label for="nomAlumno<?php $i ?>">Ingrese nombre de Alumno: </label>
            <input type="text" name="nomAlumno<?php echo $i; ?>" id="nomAlumno<?php echo $i; ?>">
            <br>
            <br>
        <?php
        }
        ?>
        <br>
        <br>
        <input type="submit" name="aceptar" id="aceptar" value="Aceptar">
        <input type="reset" name="cancelar" id="cancelar" value="Cancelar">
    </form>

    <br />
    <br />
    <a href="../index.html">Volver al menú</a>
</body>

</html>